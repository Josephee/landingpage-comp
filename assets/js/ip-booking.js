// ========================== BOOKING ================================ //
document.addEventListener("DOMContentLoaded", load, false);

function load() {
    var btnn = document.getElementById("tarifas");
    btnn.addEventListener("click", sub, false);
}

function sub() {
    // =========================================================== //
    var A_Pila = [];
    var C_Pila = [];
    // =========================================================== //
    var Adults = "";
    var Children = "";
    var checkInValue = document.getElementById("checkin").value;
    var checkOutValue = document.getElementById("checkout").value;
    var Rooms = document.getElementById('cmbNumRooms').value;
    // =========================================================== //
    if (Rooms == "2") {
        A_Pila.push(document.getElementById('iAdults2').value);
        A_Pila.push(document.getElementById('iAdults1').value);

        C_Pila.push(document.getElementById('iChildren2').value);
        C_Pila.push(document.getElementById('iChildren1').value);
    } else if (Rooms == "3") {
        A_Pila.push(document.getElementById('iAdults3').value);
        A_Pila.push(document.getElementById('iAdults2').value);
        A_Pila.push(document.getElementById('iAdults1').value);

        C_Pila.push(document.getElementById('iChildren3').value);
        C_Pila.push(document.getElementById('iChildren2').value);
        C_Pila.push(document.getElementById('iChildren1').value);
    } else {
        A_Pila.push(document.getElementById('iAdults1').value);

        C_Pila.push(document.getElementById('iChildren1').value);
    }
    // =========================================================== //
    var cont = A_Pila.length;
    for (var i = 0; i < cont; i++) {
        Adults += A_Pila.pop();
        if (A_Pila.length > 0) Adults += ",";
    }
    cont = C_Pila.length;
    for (var i = 0; i < cont; i++) {
        Children += C_Pila.pop();
        if (C_Pila.length > 0) Children += ",";
    }

    var bk = document.forms["form-booking"];
    var propertyNumber = document.getElementById("Destiny").value;
    var qs = "?PropertyNumber=" + propertyNumber;
    qs += "&Provider=0";
    qs += "&Rooms=" + document.getElementById("cmbNumRooms").value;
    qs += "&AccessCode=" + document.getElementById("txtPromotionCode").value;
    qs += "&NegociateRate=" + document.getElementById("txtNegociateRate").value;
    qs += "&Currency=MXN";
    qs += "&Adults=" + Adults;
    qs += "&Children=" + Children;
    qs += "&CheckIn=" + checkInValue;
    qs += "&CheckOut=" + checkOutValue;
    qs += "&Tab=Rates";

    var app = document.getElementById("bookingApi").value;
    var completeurl = "http://booking.internetpower.com.mx/" + app + "/hotel/hoteldescription.aspx" + qs;
    location.href = completeurl;
}

// CALENDARIO //
var currentDate = new Date();
currentDate.setMilliseconds(24 * 60 * 60 * 1000);

var opts = {
    defaultDate: "+1w",
    minDate: currentDate,
    changeMonth: true,
    dateFormat: "dd/mm/yy",
    numberOfMonths: 2,
    onSelect: function (selectedDate, inst) {
        d1 = $("#checkllegada").datepicker("getDate");
        d2 = $("#checksalida").datepicker("getDate");
        if (inst.id == "checkllegada") {
            if (d1 >= d2) {
                d2 = new Date(d1.getTime() + 86400000);
                $("#checksalida").datepicker("setDate", d2);
            }
        } else {
            if (d2 <= d1) {
                d1 = new Date(d2.getTime() - 86400000);
                $("#checkllegada").datepicker("setDate", d1);
            }
        }
        setDate("checksalida", "checkout");
        setDate("checkllegada", "checkin");
    }
};

$("#checkllegada").datepicker(
$.extend({
altField: "#checkllegada"
}, opts)
).datepicker("setDate", currentDate);
setDate("checkllegada", "checkin");

currentDate.setDate(currentDate.getDate() + 1)
$("#checksalida").datepicker(
$.extend({
altField: "#checksalida"
}, opts)
).datepicker("setDate", currentDate);
setDate("checksalida", "checkout");

function setDate(input, output) {
    var sString = $("#" + input).val();
    $("#" + output).val(sString.substring(6, 10) + sString.substring(3, 5) + sString.substring(0, 2));
}
