// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());


// JQUERY Callbacks
      $(document).on('ready', function () {

          $('.ui-datepicker-header').addClass('bg-support','color-white');

          // Datepicker
          $( "#checkllegada" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onClose: function( selectedDate ) {
              $( "#checksalida" ).datepicker( "option", "minDate", selectedDate );
            }
          });
          $( "#checksalida" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 3,
            onClose: function( selectedDate ) {
              $( "#checkllegada" ).datepicker( "option", "maxDate", selectedDate );
            }
          });

          // Toggle Content
          $('.content-btn').click(function () {
            $('.content-block').toggle();
          });

          // Social Likes
          $('.social-likes').socialLikes();

          // Content Tabs
          $('ul.tabs li').click(function(){
              var tab_id = $(this).attr('data-tab');

              $('ul.tabs li').removeClass('current');
              $('.tab-content').removeClass('current');

              $(this).addClass('current');
              $("#"+tab_id).addClass('current');
          })

          // Scroll to Top Button
          $('a.topscroll').click(function(){
            $(document.body).animate({scrollTop : 0},800);
            return false;
          });

          $(".btn").mouseover(function() {
              $(this).addClass('pulse');
            }).mouseout(function(){
              $(this).removeClass('pulse');
          });



          // RESPONSIVE
          var responsive_viewport = $(window).width();
          if (responsive_viewport >= 768) {

              /* Equalize Heights
                var maxHeight = 0;
                $(".card").each(function(){
                   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
                });
                $(".card").height(maxHeight);
              */

          };

          
      });