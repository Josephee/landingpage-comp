// Angular JS {{Principiante}}
var dateIn = document.getElementById("checkin").value;
var dateOut = document.getElementById("checkout").value;

var setting = angular.module('Settings', []);

setting.controller('CtrlSetting', ['$scope', function ($scope) {
    // == Rellena con 0 == //
    $scope.Zero = function (data) { return (data < 10) ? data = '0' + data : data = data; };

    // == Fechas == //
    $scope.FechaBooking = function (Num) {
        var today = new Date();
        today.setMilliseconds(24 * 60 * 60 * 1000);
        (Num === 1) ? today.setMilliseconds(24 * 60 * 60 * 1000) : today = today;
        var dd = today.getDate(), mm = today.getMonth() + 1, yy = today.getFullYear();
        return '' + yy + $scope.Zero(mm) + $scope.Zero(dd);
    };
    $scope.Fechas = {
        FEntrada: $scope.FechaBooking(0),
        FSalida: $scope.FechaBooking(1)
    };
    $scope.Hotel = {
        hotelName: "Hotel SPA Hacienda Baruk",
        hotelLocation: "Zacatecas, Zacatecas",
        url: "http://www.hotelesbaruk.com/nuestros-hoteles-en-zacatecas/baruk-spa-hacienda",
        hotelTel: "01 (492) 924 6666",
        hotelMail: "reservaciones.hacienda@hotelesbaruk.com",
        mejor_tarifa: "",
        telefono: "016121750860",
        callCenter: "01 (612) 175 0860"
    };
    $scope.Booking = {
        idHotel: 2203,
        parther: "HotelesBaruk",
        currency: "MXN"
    };
    $scope.Enlaces = {
        urlOffers: "http://www.hotelesbaruk.com/ofertas-hoteles-en-zacatecas",
        urlSuscription: "#",
        urlRooms: "http://www.hotelesbaruk.com/nuestros-hoteles-en-zacatecas/baruk-spa-hacienda/habitaciones",
        urlReserve: "http://booking.internetpower.com.mx/" + $scope.Booking.parther + "/hotel/hoteldescription.aspx?PropertyNumber=" + $scope.Booking.idHotel + "&Provider=0&Rooms=1&AccessCode=" +
            "&NegociateRate=&Currency=" + $scope.Booking.currency + "&Adults=2&Children=0&CheckIn=" + $scope.Fechas.FEntrada + "&CheckOut=" + $scope.Fechas.FSalida + "&Tab=Rates",
        urlNew: '&Rooms=1&PartnerId=255&StartDay=1&Nights=1&adults=2'
    };
    $scope.RedSocial = {
        facebook: "https://www.facebook.com/HotelesBaruk",
        twitter: "https://twitter.com/HotelesBaruk",
        google: " ",
        youtube: " ",
        tripadvisor: " ",
        instagram: " "
    };
    $scope.Tema = {
        colorTheme: "#1c3e5a",
        colorSupport: "#C49B1F",
        colorDefault: "#FFFFFF",
        urlFont: "https://fonts.googleapis.com/css?family=Ubuntu:400,500,700",
        fontFamily: "'Ubuntu', sans-serif;",
        radius: 0
    };

    // == Colour Brightness == //
    $scope.colourBrightness = function (hex, percent) {
        hex = hex.replace(/^\s*#|\s*$/g, '');

        if (hex.leng == 3) {
            hex = hex.replace(/(.)/g, '$1$1');
        }

        var r = parseInt(hex.substr(0, 2), 16),
            g = parseInt(hex.substr(2, 2), 16),
            b = parseInt(hex.substr(4, 2), 16);

        r = ((0 | (1 << 8) + r + (256 - r) * percent / 100).toString(16)).substr(1);
        g = ((0 | (1 << 8) + g + (256 - g) * percent / 100).toString(16)).substr(1);
        b = ((0 | (1 << 8) + b + (256 - b) * percent / 100).toString(16)).substr(1);

        return '#' + r + g + b;
    };

    // == Colores == //
    $scope.Colorea = {
        lighttheme: $scope.colourBrightness($scope.Tema.colorTheme, 30.5), //0.5
        darktheme: $scope.colourBrightness($scope.Tema.colorTheme, -8.8), //-0.8
        lightsupport: $scope.colourBrightness($scope.Tema.colorSupport, 30.8),
        darksupport: $scope.colourBrightness($scope.Tema.colorSupport, -8.9),
        lightdefault: $scope.colourBrightness($scope.Tema.colorDefault, 30.6),
        darkdefault: $scope.colourBrightness($scope.Tema.colorDefault, -8.8)
    };
}]);

setting.controller('appController', ['$scope', '$http', '$controller', function ($scope, $http, $controller) {
    //Datos Injectados
    $controller('CtrlSetting', { $scope: $scope });
    //Configuración
    var params = {
        propertyNumber: $scope.Booking.idHotel,
        currency: $scope.Booking.currency,
        app: $scope.Booking.parther,
        ext: $scope.Enlaces.urlNew
    };
    //Obtener habitaciones al cargar la página
    var _params = 'propertyNumber=' + params.propertyNumber + '&currency=' + params.currency + '&app=' + params.app + params.ext + '&callback=JSON_CALLBACK';
    var url = 'http://booking.internetpower.com.mx/DinamicRooms/Request.ashx?' + _params;
    $scope.show = false;
    $http.jsonp(url).success(function (resp) {
        setRooms(resp);
    });
    var setRooms = function (json) {
        $scope.Rooms = json.Rooms;
        angular.forEach($scope.Rooms, function (r) {
            if (parseFloat(r.total) > 0) $scope.show = true;
        });
        if (!$scope.show) console.log("Tarifas no disponibles");
    };
}]);

setting.controller('appComparator', ['$scope', '$http', '$controller', function ($, $http, $controller) {
    // Datos Injectados
    $controller('CtrlSetting', { $scope: $ });
    $controller('appController', { $scope: $ });
    $.show = false;

    // Variables locales
    var now = new Date(), d = now.getDate(), m = (now.getMonth() + 1), y = now.getFullYear(),
    params = { propertyNumber: $.Booking.idHotel, room: "" },
    checkIn = "", checkOut = "", _params = "", _url = "";
    now.setMilliseconds(24 * 60 * 60 * 1000);
    checkIn = "" + now.getFullYear() + $.Zero((now.getMonth() + 1)) + $.Zero(now.getDate());
    now.setMilliseconds(24 * 60 * 60 * 1000);
    checkOut = "" + now.getFullYear() + $.Zero((now.getMonth() + 1)) + $.Zero(now.getDate());
    _params = "PropertyNumber=" + params.propertyNumber + "&CheckIn=" + checkIn + "&CheckOut=" + checkOut + "&RoomCode=" + params.room;
    _url = "http://compare.internetpower.com.mx/reports/GetRates?" + _params;

    //Configuración
    var params = {
        propertyNumber: $.Booking.idHotel,
        currency: $.Booking.currency,
        app: $.Booking.parther,
        ext: $.Enlaces.urlNew
    };
    //Obtener habitaciones al cargar la página
    var _params = 'propertyNumber=' + params.propertyNumber + '&currency=' + params.currency + '&app=' + params.app + params.ext + '&callback=JSON_CALLBACK';
    var __url = 'http://booking.internetpower.com.mx/DinamicRooms/Request.ashx?' + _params, cont = 0;

    $http.jsonp(__url).success(function (resp) {
        $.Cuarto = resp.Rooms;
        // Llamada al servidor del comparador
        $http.get(_url).then(function (response) {
            setRooms(response.data[0].getRatesModel);
        });
    });

    // Método de obtención de datos
    var setRooms = function (array) {
        $.Rates = array;
        angular.forEach($.Rates, function (r) {
            if (r.Channel == "InternetPower") {
                r.Channel = "Nosotros";
                r.Rate = $.Cuarto[0].total.replace(",", "");
                r.Rate = parseFloat(r.Rate);
                r.Status = $.Cuarto[0].url;
            }
            if (r.Channel == "Booking.com") r.Channel = "Booking";
            if (cont < 5) $.show = true;
            cont++;
        });
        if (!$.show) console.log("No hay comparador");
    };
}]);